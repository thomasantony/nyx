---
name: Bug report
about: Create a report to help us improve
title: ''
labels: ''
assignees: ''

---

# Bug report
_Bug reports will lead to a stakeholder need report, and will need to be linked to this issue_

## Describe the bug
A clear and concise description of what the bug is.

## To Reproduce
Steps to reproduce the behavior:
1. Set up a scenario with these parameters
2. Attempted to retrieve the spacecraft mass in that way
3. Kaboom crash

## Expected behavior
A clear and concise description of what you expected to happen.

## Code to reproduce the issue
If possible, provide a short snippet of code that reproduces the bug.

## Platform
Describe the platform you encountered the bug on, e.g. desktop running Linux and calling Nyx from Python.

## Additional context
Add any other context about the problem here.
